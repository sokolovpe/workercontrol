﻿using System.Collections.Generic;
using App.Models;

namespace App
{
    public interface ISharedJobInfo
    {
        Dictionary<string, JobInfo> Infos { get; set; }
        void UpdateInfo(JobInfo info);
        JobInfo GetInfo(string name);
    }
}