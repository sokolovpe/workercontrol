﻿using System;
using System.Collections.Generic;

namespace App.Models
{
    public class JobInfo
    {
        public bool JobState { get; set; }
        public int LaunchTotalCount { get; set; }
        public int LaunchSuccessCount { get; set; }
        public int LaunchFailCount { get; set; }
        
        public DateTime LastSuccessLaunch { get; set; }
        public DateTime LastLaunch { get; set; }
        public string JobName { get; set; }
        public List<Exception> Exceptions { get; set; } = new();

        public void AddException(Exception exception)
        {
            if (Exceptions.Count == 10)
            {
                Exceptions.RemoveAt(0);
            }
            Exceptions.Add(exception);
        }
    }
}