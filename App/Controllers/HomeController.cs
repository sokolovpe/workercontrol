﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using App.Models;
using Microsoft.Extensions.Options;

namespace App.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IOptionsMonitor<FirstJobSettings> _optionsMonitor;
        private readonly ISharedJobInfo _sharedJobInfo;

        public HomeController(ILogger<HomeController> logger, IOptionsMonitor<FirstJobSettings> optionsMonitor,
            ISharedJobInfo sharedJobInfo)
        {
            _logger = logger;
            _optionsMonitor = optionsMonitor;
            _sharedJobInfo = sharedJobInfo;
        }

        public IActionResult Index()
        {
            return View();
        }
        
        public IActionResult Switch()
        {
            _optionsMonitor.CurrentValue.Enabled = !_optionsMonitor.CurrentValue.Enabled;
            return Content($"FirstJob state now is: {(_optionsMonitor.CurrentValue.Enabled ? "enabled" : "disabled")}");
        }

        public IActionResult GetInfo(string jobName)
        {
            var info = _sharedJobInfo.GetInfo(jobName);
            var json = Newtonsoft.Json.JsonConvert.SerializeObject(info);
            return new JsonResult(json);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}