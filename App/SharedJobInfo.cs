﻿using System.Collections.Generic;
using App.Models;

namespace App
{
    public class SharedJobInfo : ISharedJobInfo

    {
        public Dictionary<string, JobInfo> Infos { get; set; } = new();

        public void UpdateInfo(JobInfo info)
        {
            if (Infos.ContainsKey(info.JobName))
            {
                Infos[info.JobName] = info;
            }
            else
            {
                Infos.Add(info.JobName, info);
            }

        }

        public JobInfo GetInfo(string name)
        {
            return Infos[name];
        }
    }
}