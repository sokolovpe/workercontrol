﻿using System;
using System.Threading;
using System.Threading.Tasks;
using App.Models;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace App.BackgroundJobs
{
    public class FirstJob : BackgroundService
    {
        private readonly ILogger<FirstJob> _logger;
        private readonly IOptionsMonitor<FirstJobSettings> _settingsMonitor;
        private readonly ISharedJobInfo _sharedJobInfo;
        private JobInfo _jobInfo;

        public FirstJob(ILogger<FirstJob> logger, IOptionsMonitor<FirstJobSettings> settingsMonitor,
            ISharedJobInfo sharedJobInfo)
        {
            _logger = logger;
            _settingsMonitor = settingsMonitor;
            _sharedJobInfo = sharedJobInfo;
            _jobInfo = new JobInfo { JobName = nameof(FirstJob) };
            _sharedJobInfo.UpdateInfo(_jobInfo);
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                var launchTime = DateTime.Now;
                try
                {
                    if (_settingsMonitor.CurrentValue.Enabled)
                    {
                        _logger.LogInformation("Worker running at: {Time}", launchTime);
                        if (_jobInfo.LaunchTotalCount % 2 != 1)
                        {
                            throw new Exception($"Some exception happened at {_jobInfo.LaunchTotalCount}",
                                new ArithmeticException("Something!"));
                        }
                        _jobInfo.LaunchSuccessCount++;
                        _jobInfo.LastSuccessLaunch = launchTime;
                    }
                    else
                    {
                        _logger.LogWarning("FirstJob is disabled");
                    }
                }
                catch (Exception e)
                {
                    _jobInfo.AddException(e);
                    _jobInfo.LaunchFailCount++;
                }
                
                // Increment launches
                _jobInfo.LaunchTotalCount++;
                _jobInfo.LastLaunch = launchTime;
                _jobInfo.JobState = _settingsMonitor.CurrentValue.Enabled;
                _sharedJobInfo.UpdateInfo(_jobInfo);
                await Task.Delay(3000, stoppingToken);
            }
        }
    }
}